#!/bin/sh
set -e

if [ "$DEBUG" == "True" ] ; then
    echo "Installing development requirements..."
    pip3 install django-debug-toolbar
fi


# Wait to PosgreSQL server to be available
until PGPASSWORD=$DB_PASS psql -h $DB_HOST -U $DB_USER -c '\q' 2> /dev/null; do
  >&2 echo "Postgres is unavailable - sleeping..."
  sleep 1
done

# Runing gunicorn
if PGPASSWORD=$DB_PASS psql -h $DB_HOST -U $DB_USER -tc "SELECT 1 FROM pg_database WHERE datname = '$DB_NAME'" | grep -q 1; then
    echo "Running (just) migrations..."
    python3 manage.py migrate

else
    echo "Creating database..."
    PGPASSWORD=$DB_PASS psql -h $DB_HOST -U $DB_USER -c "CREATE DATABASE $DB_NAME;"

    echo "Running migrations..."
    python3 manage.py migrate

    export POPULATOR_MODE="True"
    echo "Populating sample data..."
    python3 manage.py populate
    export POPULATOR_MODE="False"
fi

echo "Collect staticfiles ..."
python3 manage.py collectstatic --noinput

echo "Apply changelogs ..."
python3 manage.py applychange

echo "Runing worker ..."
celery -A service worker -l info &

echo "Starting autobackup ..."
python3 schedule_task.py &

echo "Runing gunicorn"
gunicorn service.wsgi:application  --workers 2 --timeout 30 --log-level=INFO --log-file=- --bind=0.0.0.0:8000 &

# Avoid exiting this script (PID 1)
tail -f /dev/null


