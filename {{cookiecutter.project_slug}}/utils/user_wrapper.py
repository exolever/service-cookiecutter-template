from django.contrib.auth import get_user_model
from django.conf import settings


class UserWrapper:
    user = None
    user_data = {}

    def __init__(self, uuid):
        self.get_user(uuid)
        self.user, _ = get_user_model().objects.get_or_create(
            uuid=self.user_data.get('uuid'))

    def get_user(self, uuid):
        self.user_data = get_user_model().objects.retrieve_remove_user_data_by_uuid(uuid)

    @property
    def is_consultant(self):
        return self.consultant_id is not None

    def get_full_name(self):
        return self.full_name

    @property
    def can_receive_new_opportunities(self):
        return True

    @property
    def groups(self):
        return [group['name'] for group in self.user_data.get('groups')]

    @property
    def is_delivery_manager(self):
        return settings.OPPORTUNITIES_DELIVERY_MANAGER_GROUP in self.groups

    def __getattr__(self, name):
        if name in self.user_data:
            return self.user_data.get(name)
        else:
            raise AttributeError
