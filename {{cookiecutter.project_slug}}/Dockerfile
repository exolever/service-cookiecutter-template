FROM alpine:3.9 as {{cookiecutter.project_slug}}

LABEL maintainer="DevOps Team <devops@openexo.com>"
LABEL vendor="openexo"

WORKDIR /projects/{{cookiecutter.project_slug}}

ARG REPO_SLUG
ARG SOURCE_BRANCH
ARG SOURCE_TAG

ENV REPO_SLUG=${REPO_SLUG} \
    SOURCE_BRANCH=${SOURCE_BRANCH} \
    SOURCE_TAG=${SOURCE_TAG} \
    PYTHONUNBUFFERED=1 \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    PIPENV_YES=1 \
    DB_NAME={{cookiecutter.project_slug.replace('-', '_')}} \
    DB_USER=exolever \
    DB_PASS=exolever \
    DB_HOST=postgres \
    DB_PORT=5432

# Adding just Pipfile (and freeze.txt if exists) to  optimize docker layer cache build
COPY Pipfile freeze*.txt ./

# Installing alpine base packages
RUN apk add --no-cache \
        python3 \
        py3-openssl \
        py3-cffi \
        py3-cryptography \
        py3-requests \
        py3-psycopg2 \
        postgresql-client 

# Adding temporary packages, pip installing and cleaning
RUN apk add --no-cache --virtual .build-deps  \
        build-base \
        python3-dev \
        libffi-dev \
        git \
        postgresql-dev \
        zlib-dev \
    && \
    pip3 install --upgrade pip \
    && \
        if [ ! -z $SOURCE_TAG ]; then \
            pip3 install -r freeze.txt; \
        else \
            pip3 install pipenv; \
            pipenv install --system --skip-lock; \
        fi \
    && \
    && \
    apk del .build-deps --force-broken-world \
    && \
    find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
        \) -exec rm -rf '{}' + \
    && rm -rf /usr/src/python

# # Check packages, when them fix this https://github.com/pypa/pipenv/issues/2783
# RUN pipenv check --system

# Running collectstatic of django
RUN python3 manage.py collectstatic --noinput

# Show package versions
RUN pip3 freeze

# Copying rest of files
COPY . .

# Setting local.py
RUN cp service/local.py.dist service/local.py

# Running basic check of django
RUN python3 manage.py check

EXPOSE 8000

CMD sh -f run.sh
