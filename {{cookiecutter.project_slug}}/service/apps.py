# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    'django_celery_results',
    'django_extensions',
    'health_check',
    'auth_uuid.apps.AuthConfig',
    'rest_framework',
    'corsheaders',
    'drf_yasg',
    'populate',
    'exo_changelog',
    '{{cookiecutter.app_name}}',
]
