from celery import current_app

FORCE_SCRIPT_NAME = ''
METRIC_URL = ''

# Statics

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

# Celery

current_app.conf.task_always_eager = True
current_app.conf.task_eager_propagates = True
