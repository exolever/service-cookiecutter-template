from django.contrib.auth import get_user_model

from {{cookiecutter.app_name}}.models import {{cookiecutter.app_model_name}}

from populate.populator.builder import Builder


User = get_user_model()


class {{cookiecutter.app_model_name}}Builder(Builder):

    def create_object(self):
        pass
