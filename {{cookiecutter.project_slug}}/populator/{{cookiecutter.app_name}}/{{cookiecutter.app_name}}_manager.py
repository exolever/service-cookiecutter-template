from singleton_decorator import singleton

from {{cookiecutter.app_name}}.models import {{cookiecutter.app_model_name}}

from populate.populator.manager import Manager
from .{{cookiecutter.app_name}}_builder import {{cookiecutter.app_model_name}}Builder


@singleton
class {{cookiecutter.app_model_name}}Manager(Manager):
    model = {{cookiecutter.app_model_name}}
    attribute = 'title'
    builder = {{cookiecutter.app_model_name}}Builder
    files_path = '/{{cookiecutter.app_name}}/files/'
