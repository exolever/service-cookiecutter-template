from populate.populator.common.errors import ConfigurationError
from .{{cookiecutter.app_name}}_manager import {{cookiecutter.app_model_name}}Manager


def {{cookiecutter.app_model_name.lower()}}_constructor(loader, node):
    item = loader.construct_mapping(node=node)
    if not isinstance(item, dict) or not item:
        raise ConfigurationError(
            'value {} cannot be interpreted as an opportunity'.format(item))
    return {{cookiecutter.app_model_name}}Manager().get_object(item)
