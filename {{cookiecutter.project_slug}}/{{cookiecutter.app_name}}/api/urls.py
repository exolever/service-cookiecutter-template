from rest_framework import routers

app_name = '{{cookiecutter.app_name}}'

router = routers.SimpleRouter()

urlpatterns = router.urls
