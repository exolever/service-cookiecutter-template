from django.db import models

from utils.models import CreatedByMixin
from model_utils.models import TimeStampedModel


# Create your models here.
class {{cookiecutter.app_model_name}}(
        CreatedByMixin,
        TimeStampedModel):
    pass
